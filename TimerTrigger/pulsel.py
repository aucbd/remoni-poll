from typing import Tuple

import requests


base_url: str = "http://pulseuiapi.azurewebsites.net/api/"


def authenticate(username: str, password: str) -> Tuple[str, int]:
    headers: dict = {"Content-Type": "application/json", "Accept": "application/json"}
    data: dict = {"User": username, "Password": password}

    res: requests.Response = requests.post(
        base_url + "user/login", headers=headers, json=data
    )

    try:
        json = res.json()
        return json.get("Token", ""), res.status_code
    except:
        return "", res.status_code


def post_data(auth: str, device_id: str, data_type: str, data: dict) -> int:
    headers: dict = {
        "Content-Type": "application/json",
        "Accept": "application/json",
        "X-Authorization": auth,
    }

    payload: dict = {
        "Id": data_type,
        "Value": data["Value"],
        "Device": device_id,
    }

    if "Time" in data:
        payload["Time"] = data["Time"]

    res: requests.Response = requests.post(
        base_url + f"messages", headers=headers, json=payload
    )

    return res.status_code
