from base64 import encodebytes
from datetime import datetime, timedelta
from typing import Dict, List, Tuple, Union

import requests

base_url: str = "https://api.remoni.com/v1/"


def generate_auth(username: str, password: str) -> str:
    """
    Given username and password, it returns a base 64 encoded string of the two
    separated by a colon.

    :param username: the username
    :param password: the password
    """
    return encodebytes(f'{username}:{password}'.encode()).decode().strip()


def get_units(auth: str) -> Tuple[List[Dict], int]:
    """
    Given an auth token, calls `/Units` on the Remoni API and returns a list of unit
    objects together with the status code of the request.

    If the API call fails, an empty list is returned instead.

    :param auth: base 64 encoded username and password separated with a colon.
    """

    res: requests.Response = requests.get(
        base_url + "Units", headers={"Authorization": f"basic {auth}"}
    )

    if res.status_code != 200:
        return [], res.status_code

    return res.json(), res.status_code


def get_data(
    auth: str, unit: Dict, date: datetime = datetime.utcnow(), interval: timedelta = None
) -> Tuple[List[Dict], int]:
    """
    Given an auth token and a unit object (and optional end date and time interval),
    calls `/Data` on the Remoni API and returns a list of datapoints from the specified
    unit.

    If the API call fails, an empty list is returned instead.

    :param auth: base 64 encoded username and password separated with a colon.
    :param unit: a dict containing at least "UnitId" and "DataInterval" keys.
    :param date: a datetime.datetime object, datapoints will be retrieved from before
    its date. Defaults to the moment the function is called.
    :param interval: a datetime.timedelta object, datapoints will be retrieved between
    `date` and `date - interval` (inclusive).
    """

    params: Dict[str, Union[str, Tuple[str, ...]]] = {
        "UnitId": f"eq({unit['UnitId']})",
        "AggregateType": f"eq(Minutes5)",
        "Timestamp": f"le({date.isoformat()}Z)"
        if not interval
        else (f"le({date.isoformat()}Z)", f"ge({(date - interval).isoformat()}Z)"),
    }

    res: requests.Response = requests.get(
        base_url + "Data", params=params, headers={"Authorization": f"basic {auth}"}
    )

    if res.status_code != 200:
        return [], res.status_code

    return res.json(), res.status_code
