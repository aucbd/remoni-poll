import datetime
import logging
from os import environ

import azure.functions as func

from .remoni import generate_auth, get_data, get_units
from .pulsel import authenticate, post_data


class AuthenticationError(BaseException):
    pass


class APIError(BaseException):
    pass


def main(mytimer: func.TimerRequest) -> None:
    utc_date = datetime.datetime.utcnow()

    utc_timestamp = utc_date.replace(tzinfo=datetime.timezone.utc).isoformat()

    utc_date = datetime.datetime(
        utc_date.year, utc_date.month, utc_date.day, utc_date.hour, utc_date.minute
    )

    if mytimer.past_due:
        logging.info("The timer is past due!")

    logging.info("Python timer trigger function ran at %s", utc_timestamp)

    username_remoni: str = environ["REMONI_USERNAME"]
    password_remoni: str = environ["REMONI_PASSWORD"]
    username_pulsel: str = environ["PULSEAPI_USERNAME"]
    password_pulsel: str = environ["PULSEAPI_PASSWORD"]

    auth_remoni = generate_auth(username_remoni, password_remoni)
    auth_swagger, res_code = authenticate(username_pulsel, password_pulsel)

    if res_code != 200:
        raise AuthenticationError(f"ERROR: swagger authentication {res_code}")

    timestamps: list = []

    units, res_code = get_units(auth_remoni)

    if res_code != 200:
        raise APIError(f"ERROR: remoni get units {res_code}")

    logging.info(f"Units: {[unit['UnitId'] for unit in units]}")

    for unit in units:
        logging.info(f"Unit: {unit['UnitId']}")
        data, res_code = get_data(
            auth_remoni, unit, date=utc_date, interval=datetime.timedelta(minutes=15)
        )

        if res_code != 200:
            raise APIError(f"ERROR: remoni get data {res_code}")

        logging.info(f"Data: {data}")

        for datapoint in data:
            post_data(
                auth_swagger,
                f"REMONI{int(unit['UnitId']):06}",
                datapoint["DataType"],
                datapoint,
            )
